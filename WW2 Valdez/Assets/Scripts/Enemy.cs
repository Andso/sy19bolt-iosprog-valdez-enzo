﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    // Stats
    [Header("Stats")]
    public List<Transform> WayPoints;
    public float startHealth = 100;
    private float health;
    public float startSpeed = 10f;
    private float speed;
    


    // Movement 
    [Header("Movement")]
    private Transform target;
    private int waypointIndex = 0;

    private bool enemyIsDead = false;

    // Health Bar
    [Header("Health Bar")]
    public Image healthBar;


    // Start is called before the first frame update
    void Start()
    {
        speed = startSpeed;
        health = startHealth;
        target = WayPoints[0];
    }

    // Update is called once per frame
    void Update()
    {
        GoToBase();
    }

    void GoToBase()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if(Vector3.Distance(transform.position , target.position) <= 0.2f)
        {
            waypointIndex++;
            if (waypointIndex >= WayPoints.Count)
            {
                // Reached the waypoint target
                // Reduce player base health
                Destroy(gameObject);
            }
            else
            {
                target = WayPoints[waypointIndex];
            }
        }
    }

    public void TakeDamage(float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / startHealth;

        if (health <= 0 && !enemyIsDead)
        {
            EnemyDeath();
        }
    }


    void EnemyDeath()
    {
        enemyIsDead = true;

        Destroy(gameObject);
    }
}
