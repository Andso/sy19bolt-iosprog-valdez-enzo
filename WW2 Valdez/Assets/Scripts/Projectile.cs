﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Stats")]
    public float Speed = 50f;
    public int Damage = 50;

    protected Transform target;

    // Start is called before the first frame update

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (target == null)
        {
            DestroyProjectile();
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = Speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            DamageEnemy(target);
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
    }


    protected virtual void DamageEnemy(Transform enemy)
    {
        Enemy eHealth = enemy.GetComponent<Enemy>();

        if (eHealth != null)
        {
            eHealth.TakeDamage(Damage);
        }
    }


    protected void DestroyProjectile()
    {
        Destroy(gameObject);
    }

}

