﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public WaypointController WaypointController;
    public GameObject EnemyPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Spawn()
    {
        // Instantiate an enemy prefab
        GameObject enemyClone = Instantiate(EnemyPrefab);
        // Get the Enemy.cs component
        Enemy enemyScript = enemyClone.GetComponent<Enemy>();
        // Assign waypoints to enemy here
    }
}
