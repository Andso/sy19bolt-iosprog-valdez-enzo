﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Stats
    public List<Transform> WayPoints;
    public float Speed = 0;
    public float Health = 0;
    public float StartFireRate;

    // Moving
    private Transform target;
    private int waypointIndex = 0;
    private float fireRate;

    private bool enemyIsDead = false;

    // Start is called before the first frame update
    void Start()
    {
        target = WayPoints[0];
    }

    // Update is called once per frame
    void Update()
    {
        GoToBase();
    }

    void GoToBase()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * Speed * Time.deltaTime, Space.World);

        if(Vector3.Distance(transform.position , target.position) <= 0.2f)
        {
            waypointIndex++;
            if (waypointIndex >= WayPoints.Count)
            {
                // Reached the waypoint target
                // Reduce player base health
                Destroy(gameObject);
            }
            else
            {
                target = WayPoints[waypointIndex];
            }
        }
    }


    public void TakeDamage(float amount)
    {
        Health -= amount;

        if (Health <= 0 && !enemyIsDead)
        {
            EnemyDeath();
        }
    }

    void EnemyDeath()
    {
        enemyIsDead = true;

        Destroy(gameObject);
    }
}
