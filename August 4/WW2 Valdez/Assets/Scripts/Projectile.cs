﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public float speed;

    private Transform Tower;
    private Vector3 target;

    // Start is called before the first frame update
    void Start()
    {
        Tower = GameObject.FindGameObjectWithTag("Tower").transform;

        target = new Vector3(Tower.position.x, Tower.position.y, Tower.position.z);

    }

    // Update is called once per frame
    void Update()
    {

        transform.position = Vector3.MoveTowards(transform.position, Tower.position, speed * Time.deltaTime);

        if(transform.position.x == target.x && transform.position.y == target.y && transform.position.z == target.z)
        {
            DestroyProjectile();


        }

    }

    void OnTriggerEnter3D(Collider other)
    {
        if(other.CompareTag("Tower"))
        {
            DestroyProjectile();

        }


    }

    void DestroyProjectile()
    {
        Destroy(gameObject);

    }

}

