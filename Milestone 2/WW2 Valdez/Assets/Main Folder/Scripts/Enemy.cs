﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Stats
    public float speed = 0;
    public float health = 100;
    private float fireRate;
    public float startFireRate;
    // Moving
    private Transform target;
    public int waypointIndex = 0;
    public int endwaypointIndex = 0;
    public int stoppingDistance;

    public GameObject bulletProjectile;

    public Transform tower;
    private bool EnemyisDead = false;
    public Transform waypoint;

    // Start is called before the first frame update
    void Start()
    {
        speed = 10;
        health = 100;
        tower = GameObject.FindGameObjectWithTag("Tower").transform;
        fireRate = startFireRate;
        waypoint = Waypoint.points[endwaypointIndex];
    }

    // Update is called once per frame
    void Update()
    {
    
        if (Vector3.Distance(transform.position, tower.position) < stoppingDistance)
        {
            transform.position = this.transform.position;


        }

        AimAtTarget();

         if (fireRate <= 0)
         {
                Instantiate(bulletProjectile, transform.position, Quaternion.identity);
                fireRate = startFireRate;
         }
         else
         {
                fireRate -= Time.deltaTime;
         }

        GoToBase();
    }

    void GoToBase()
    {
        Vector3 dir = waypoint.position - transform.position;
        transform.Translate(dir.normalized* speed * Time.deltaTime, Space.World);
    }

    void AimAtTarget()
    {
        Vector3 aim = tower.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(aim);
        transform.rotation = rotation;

    }

    public void TakeDamage(float amount)
    {
        health -= amount;


        if (health <= 0 && !EnemyisDead)
        {
            EnemyDeath();
        }

    }


    void EnemyDeath()
    {
        EnemyisDead = true;

        Destroy(gameObject);

    }

}
