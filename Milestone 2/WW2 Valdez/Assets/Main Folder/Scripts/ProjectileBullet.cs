﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBullet : MonoBehaviour
{ 
    public  GameObject materialBullet;


     public  void Create(Vector3 spawnPosition)
     {

        Instantiate(materialBullet.gameObject, spawnPosition, Quaternion.identity);

     }


    // Start is called before the first frame update
    void Start()
    {

    }
      // Update is called once per frame
    void Update()
    {

    }
}
