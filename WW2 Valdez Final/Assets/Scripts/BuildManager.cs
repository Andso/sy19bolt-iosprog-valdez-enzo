﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class BuildManager : MonoBehaviour
{

	public static BuildManager instance;
	public Vector3 positionOffset;

	[Header("Towers")]
	private TowerCost towerBlueprint;
	private GameObject turret;
	public GameObject machineGun;
	public GameObject upgradedmachineGun;

	public GameObject rocketGun;
	public GameObject upgradedrocketGun;

	public GameObject laserGun;
	public GameObject upgradedlaserGun;



	public bool CanBuild { get { return towerBlueprint != null; } }
	public bool HasMoney { get { return Base.Money >= towerBlueprint.cost; } }


    void Update()
    {
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(ray.origin, ray.direction, out hit))
			{

				Instantiate(turret, hit.point, turret.transform.rotation);

			}
		}

		if (Input.GetKeyDown("u"))
		{
			turret = machineGun;
		}
		if (Input.GetKeyDown("i"))
		{
			turret = upgradedmachineGun;
		}
		if (Input.GetKeyDown("j"))
		{
			turret = rocketGun;
		}
		if (Input.GetKeyDown("k"))
		{
			turret = upgradedrocketGun;
		}
		if (Input.GetKeyDown("n"))
		{
			turret = laserGun;
		}
		if (Input.GetKeyDown("m"))
		{
			turret = upgradedlaserGun;
		}

	}





	public void SelectTurretToBuild(TowerCost blueprint)
	{
		towerBlueprint = blueprint;
	}

	public TowerCost GetTurretToBuild()
	{
		return towerBlueprint;
	}
}
