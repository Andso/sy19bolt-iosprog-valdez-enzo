﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

	public static bool GameIsOver;

	public GameObject gameOverUI;
	public GameObject completeLevelUI;

	void Start()
	{
		GameIsOver = false;
	}
	// Update is called once per frame
	void Update()
	{
		if (GameIsOver)
			return;
		if (Base.Health <= 0)
		{
			GameOver();
		}
	}

	void GameOver()
	{

		SceneManager.LoadScene("GameOver", LoadSceneMode.Single);

	}

	public void WinLevel()
	{
		SceneManager.LoadScene("GameWin", LoadSceneMode.Single);


	}
}
