﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketProjectile : MonoBehaviour
{
   
  
    [Header("Stats")]
    public float Speed = 50f;
    public int Damage = 50;
    public int AOERadius = 5;

    protected Transform target;

    // Start is called before the first frame update

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (target == null)
        {
            DestroyProjectile();
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = Speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);

    }


    protected virtual void DamageEnemy(Transform enemy)
    {
        Enemy eHealth = enemy.GetComponent<Enemy>();

        if (eHealth != null)
        {
            eHealth.TakeDamage(Damage);
        }
    }
    protected virtual void HitTarget()
    {
        Explode();

        Destroy(gameObject);
    }
    void Explode()
    {
        
        Collider[] colliders = Physics.OverlapSphere(transform.position, AOERadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                DamageEnemy(collider.transform);
            }
        }
    }
    protected void DestroyProjectile()
    {
        Destroy(gameObject);
    }

}
