﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [Header("Stats")]
    public float Speed = 50f;
    public int Damage = 50;
    public float AOERadius = 0f;
    protected Transform target;

    // Start is called before the first frame update

    public void Seek(Transform _target)
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            DestroyProjectile();
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = Speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);

    }


   void DamageEnemy(Transform enemy)
    {
        Enemy eHealth = enemy.GetComponent<Enemy>();

        if (eHealth != null)
        {
            eHealth.TakeDamage(Damage);
        }
    }
    void HitTarget()
    {
        if (AOERadius > 0f)
        {
            Explode();
        }
        else
        {
             DamageEnemy(target);
        }
          

        Destroy(gameObject);
    }
    void Explode()
    {

        Collider[] colliders = Physics.OverlapSphere(transform.position, AOERadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                Debug.Log("PLEASE EXPLODE!!");
                DamageEnemy(collider.transform);
            }
        }

    }
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AOERadius);
    }

   void DestroyProjectile()
    {
        Destroy(gameObject);
    }

}

