﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]

public class WaveEnemies 
{


	// Types of enemies being spawned every wave
	public GameObject enemy;
	// Enemies Alive
	public int count;
	// Time for another Wave spawned
	public float rate;


}
