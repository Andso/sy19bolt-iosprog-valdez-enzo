﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    // Stats
    [Header("Stats")]
    public float startHealth = 100;
    private float health;
    public float startSpeed = 10f;
    private float speed;
    private float rotspeed = 10f;
    public int worth = 0;

    // Movement 
    [Header("Movement")]
    private Transform target;
    private int waypointIndex = 0;

    private bool enemyIsDead = false;

    // Health Bar
    [Header("Health Bar")]
    public Image healthBar;


    // Start is called before the first frame update
    void Start()
    { 
        speed = startSpeed;
        health = startHealth;
        target = WaypointController.points[0];
    }

    // Update is called once per frame
    void Update()
    {
        GoToBase();
    }

    void GoToBase()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
                                                    Quaternion.LookRotation(dir),
                                                    Time.deltaTime * rotspeed);
        if (Vector3.Distance(transform.position , target.position) <= 0.4f)
        {
            if (waypointIndex >= WaypointController.points.Length)
            {
                Base.Health -= health;
                WaveSpawner.EnemiesAlive--;
                Destroy(gameObject);
                return;
            }
            else
            {
                target = WaypointController.points[waypointIndex];
                waypointIndex++;
            }
           
        }
    }
    public void Slow(float pct)
    {
        speed = startSpeed * (1f - pct);
    }

    public void TakeDamage(float amount)
    {

        health -= amount;

        healthBar.fillAmount = health / startHealth;

        if (health <= 0 && !enemyIsDead)
        {
            EnemyDeath();
        }
    }


    void EnemyDeath()
    {
        enemyIsDead = true;
        Base.Money += worth;
        WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);
    }
}
