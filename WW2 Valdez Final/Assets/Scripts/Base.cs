﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{

	public static float Health;
	public int startHealth = 20;
	public static int Money;
	public int startMoney = 400;

	public static int Rounds;

	void Start()
	{
		Money = startMoney;
		Health = startHealth;
		Rounds = 0;
	}

}
